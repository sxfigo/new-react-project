import React from "react";
import { render, fireEvent, screen, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import "@testing-library/jest-dom/extend-expect";
import { Provider } from "react-redux";
import store from "../redux/store";
import ProgramPage from "../pages/ProgramPage";
import { BrowserRouter, Route, Switch } from "react-router-dom";

test("show carousel", async () => {
  const { getByTestId } = render(
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/program/56197" component={ProgramPage} />
        </Switch>
      </BrowserRouter>
    </Provider>
  );

  // await waitFor(() => {
  screen.debug();
  expect(getByTestId("test-program")).toBeInTheDocument();
  // });
});
